package in.silentsudo.springcloud.users;

import com.fasterxml.jackson.annotation.JsonFormat;
import in.silentsudo.springcloud.users.storage.UserDatastore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@SpringBootApplication
public class UserMicroServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(UserMicroServiceApp.class, args);
    }

    @RestController
    @RequestMapping(path = "/users")
    static class UserController {
        public static final String USER_CREATED_BINDING_NAME = "userCreated";

        private final StreamBridge streamBridge;
        private final UserDatastore userDatastore;

        @Autowired
        UserController(StreamBridge streamBridge, UserDatastore userDatastore) {
            this.streamBridge = streamBridge;
            this.userDatastore = userDatastore;
        }

        @PostMapping
        public Map<String, String> create(@RequestBody User user) {
            final UserCreatedEvent userCreatedEvent = UserCreatedEvent.generateForUser(user);
            userDatastore.add(userCreatedEvent.getUser());
            final boolean sent = streamBridge.send(USER_CREATED_BINDING_NAME, userCreatedEvent);
            System.out.printf("Message sent %s%n", sent);
            return Map.of("status", "success");
        }

        @DeleteMapping("/{username}")
        public Map<String, Object> delete(@PathVariable("username") String username) {
            final Optional<User> removedUser = userDatastore.remove(username);
            if (removedUser.isPresent()) {
                return Map.of("status", "success", "user", removedUser.orElse(null));
            } else {
                return Map.of("status", "failure");
            }
        }

        @GetMapping
        public Map<String, Collection<User>> users() {
            return Map.of("users", userDatastore.all());
        }
    }


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    static class UserCreatedEvent {
        private String eventUuId;
        private User user;

        public static UserCreatedEvent generateForUser(User user) {
            if (user != null && !StringUtils.hasText(user.getUuid())) {
                user.setUuid(UUID.randomUUID().toString());
            }
            return new UserCreatedEvent(UUID.randomUUID().toString(), user);
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class User {
        private String uuid;
        private String username;
        @JsonFormat(pattern = "dd-MM-yyyy", shape = JsonFormat.Shape.STRING)
        private LocalDate dateOfBirth;

        public static User generateForUsername(String username, LocalDate dob) {
            return new User(UUID.randomUUID().toString(), username, dob);
        }
    }
}
