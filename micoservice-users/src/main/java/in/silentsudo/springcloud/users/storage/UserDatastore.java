package in.silentsudo.springcloud.users.storage;

import in.silentsudo.springcloud.users.UserMicroServiceApp;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.Optional;

public interface UserDatastore {
    default void add(UserMicroServiceApp.User user) {
        if (user != null && StringUtils.hasText(user.getUsername()) && !exists(user.getUsername())) {
            store(user);
        }
    }

    default Optional<UserMicroServiceApp.User> remove(String username) {
        if (StringUtils.hasText(username)) {
            return removeFromDataStore(username);
        }
        return Optional.empty();
    }

    void store(UserMicroServiceApp.User user);

    Optional<UserMicroServiceApp.User> removeFromDataStore(String username);

    boolean exists(String username);

    Collection<UserMicroServiceApp.User> all();

}
