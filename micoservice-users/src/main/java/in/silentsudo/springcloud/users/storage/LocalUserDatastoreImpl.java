package in.silentsudo.springcloud.users.storage;

import in.silentsudo.springcloud.users.UserMicroServiceApp;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class LocalUserDatastoreImpl implements UserDatastore {
    private final Map<String, UserMicroServiceApp.User> users;

    public LocalUserDatastoreImpl() {
        users = new ConcurrentHashMap<>();
    }

    @Override
    public void store(UserMicroServiceApp.User user) {
        users.put(user.getUsername(), user);
    }

    @Override
    public Optional<UserMicroServiceApp.User> removeFromDataStore(String username) {
        if (exists(username)) {
            return Optional.of(users.remove(username));
        }
        return Optional.empty();
    }

    @Override
    public boolean exists(String username) {
        return users.containsKey(username);

    }

    @Override
    public Collection<UserMicroServiceApp.User> all() {
        return users.values();
    }
}
