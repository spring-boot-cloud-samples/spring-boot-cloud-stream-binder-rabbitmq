package in.silentsudo.springcloud.birthdayservice;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class ScheduleConfig {
    private final BirthdayReminderService birthdayReminderService;

    public ScheduleConfig(BirthdayReminderService birthdayReminderService) {
        this.birthdayReminderService = birthdayReminderService;
    }

    @Scheduled(initialDelay = 3000, fixedDelay = 5000)
    public void birthdayFinderSchedule() {
        System.out.println("************* Birthdays today ****************");
        System.out.println(birthdayReminderService.listTodaysBirthday());
        System.out.println("**********************************************");
    }

    @Scheduled(initialDelay = 3000, fixedDelay = 5000)
    public void upcomingBirthdaysSchedule() {
        System.out.println("************ Upcoming Birthdays **************");
        System.out.println(birthdayReminderService.listUpcomingBirthdays());
        System.out.println("**********************************************");
    }
}
