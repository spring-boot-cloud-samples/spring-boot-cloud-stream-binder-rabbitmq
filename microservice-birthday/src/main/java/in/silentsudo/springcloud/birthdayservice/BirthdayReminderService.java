package in.silentsudo.springcloud.birthdayservice;

import in.silentsudo.springcloud.birthdayservice.models.BirthdayReminder;
import in.silentsudo.springcloud.birthdayservice.models.UserCreatedEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.logging.Logger;

@Service
public class BirthdayReminderService {
    private final Logger logger = Logger.getLogger(BirthdayReminderService.class.getSimpleName());
    private final BirthdayDatastore birthdayDatastore;

    public BirthdayReminderService(BirthdayDatastore birthdayDatastore) {
        this.birthdayDatastore = birthdayDatastore;
    }

    public void add(BirthdayReminder reminder) {
        birthdayDatastore.add(reminder);
    }

    public List<BirthdayReminder> listUpcomingBirthdays() {
        return birthdayDatastore.listUpcomingBirthdays();
    }

    public List<BirthdayReminder> listTodaysBirthday() {
        return birthdayDatastore.listTodaysBirthday();
    }

    @Bean
    public Consumer<UserCreatedEvent> userCreated() {
        return userCreatedEvent -> {
            logger.info(userCreatedEvent.toString());
            BirthdayReminder birthdayReminder =
                    new BirthdayReminder(UUID.randomUUID().toString(), false, userCreatedEvent.getUser());
            add(birthdayReminder);
        };
    }
}
