package in.silentsudo.springcloud.birthdayservice.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BirthdayReminder {
    private String uuid;
    private boolean remind;
    private User user;
}