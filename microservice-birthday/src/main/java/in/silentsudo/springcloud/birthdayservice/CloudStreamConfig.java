//package in.silentsudo.springcloud.birthdayservice;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.stereotype.Component;
//
//import java.util.function.Consumer;
//
//@Configuration
//@Component
//public class CloudStreamConfig {
//
////    private final BirthdayReminderService birthdayReminderService;
////
////    public CloudStreamConfig(BirthdayReminderService birthdayReminderService) {
////        this.birthdayReminderService = birthdayReminderService;
////    }
//
//    @Bean
//    public Consumer<BirthdayMicroServiceApp.UserCreatedEvent> userCreated() {
//        return userCreatedEvent -> {
////            BirthdayMicroServiceApp.BirthdayReminder birthdayReminder =
////                    new BirthdayMicroServiceApp.BirthdayReminder(UUID.randomUUID().toString(), false, userCreatedEvent.getUser());
////            birthdayReminderService.add(birthdayReminder);
//            System.out.println(userCreatedEvent);
//        };
//    }
//}
