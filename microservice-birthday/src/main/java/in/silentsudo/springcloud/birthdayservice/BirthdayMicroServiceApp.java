package in.silentsudo.springcloud.birthdayservice;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;

@SpringBootApplication
public class BirthdayMicroServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(BirthdayMicroServiceApp.class, args);
    }
}
