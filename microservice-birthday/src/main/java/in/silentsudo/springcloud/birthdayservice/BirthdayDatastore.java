package in.silentsudo.springcloud.birthdayservice;

import in.silentsudo.springcloud.birthdayservice.models.BirthdayReminder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class BirthdayDatastore {
    private final Map<String, BirthdayReminder> users;

    public BirthdayDatastore() {
        users = new ConcurrentHashMap<>();
    }

    public void add(BirthdayReminder birthdayReminder) {
        users.put(birthdayReminder.getUser().getUsername(), birthdayReminder);
    }

    public void remove(BirthdayReminder birthdayReminder) {
        final String username = birthdayReminder.getUser().getUsername();
        System.out.printf("removing user %s%n", username);
        if (exists(username)) {
            users.remove(username);
        }
    }

    public boolean exists(String username) {
        return users.containsKey(username);
    }

    public List<BirthdayReminder> listTodaysBirthday() {
        final LocalDate today = LocalDate.now();
        return list(birthdayReminder -> today.isEqual(birthdayReminder.getUser().getDateOfBirth()));
    }

    public List<BirthdayReminder> listUpcomingBirthdays() {
        final LocalDate today = LocalDate.now();
        return list(birthdayReminder -> birthdayReminder.getUser().getDateOfBirth().isAfter(today));
    }

    public List<BirthdayReminder> list(Predicate<BirthdayReminder> predicate) {
        return users.entrySet()
                .stream()
                .parallel()
                .map(Map.Entry::getValue)
                .filter(predicate)
                .collect(Collectors.toUnmodifiableList());
    }
}
