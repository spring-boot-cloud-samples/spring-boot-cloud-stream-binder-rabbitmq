# Getting Started

## Install rabbitmq

```sh
docker run -d --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management
```

## Build

```sh
./gradlew clean build
```
## Run
```sh
- java -jar /microservice-message-broker-comm/micoservice-users/build/libs/micoservice-users-0.0.1-SNAPSHOT.jar
- java -jar /microservice-message-broker-comm/microservice-birthday/build/libs/microservice-birthday-0.0.1-SNAPSHOT.jar
```

## Add data
user a
```curl
curl --location --request POST 'localhost:7001/users' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username":"raja",
    "dateOfBirth":"16-05-2021"
}'
```

user b
```curl
curl --location --request POST 'localhost:7001/users' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username":"rancho",
    "dateOfBirth":"21-05-2021"
}'
```

## Output
![gif](event-driven-microservice-output.gif)

